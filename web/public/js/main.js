/*!
* Este archivo es parte del proyecto app_organigrama
* 
* Este sistema esta encargado de crear organigramas basicos y personalizados y la gestion de ellos.
* 
* Replicando un organigrama que hice para apoyar en un proyecto a mi papá
* 
* @copyright Copyright (c);
* 
* @filesource
* 
* Hecho con <3 por Ing. Glendy Covarrubias
* 
* DEPENDENCIES:
* JQUERY
* Font Awesome
* 
* 
*/
'use strict';

$(document).ready(function () {
    /*Botonera*/
    $('#botonera').appendTo('.nav');

    //ocultando boton de filtere tabla
    $('body > div.container-fluid > section > section > div > div.panel-body > div > div.bootstrap-table > div.fixed-table-toolbar > div:nth-child(2) > button').css('display', 'none');

    //Tabla filter
    $('#ItemStatusfilter1').append('<option id="Libre" value="Libre">Libre</option');
    $('#ItemStatusfilter1').append('<option id="Personalizado" value="Personalizado">Personalizado</option');

    //Elementos
    var elements = {
        button: {
            Libre: '#btnLibre',
            personalizado: '#btnPersonalizado'
        }
    };

    //Boton de organigrama libre
    $('body').on('click', elements.button.Libre, function () {
        var url = Routing.generate('vista_organigrama_libre');

        $.ajax({
            url: url
        }).done(function () {
            console.log(url);
            console.log('organigrama libre');
            window.location.href = url;
            location.href = url;
            /************************ INICIO ***********************************************/
            // let datascource = {
            //       'name': 'Lao Lao',
            //       'title': 'general manager',
            //       'children': [
            //         { 'name': 'Bo Miao', 'title': 'department manager',
            //           'children': [{ 'name': 'Li Xin', 'title': 'senior engineer' }]
            //         },
            //         { 'name': 'Su Miao', 'title': 'department manager',
            //           'children': [
            //             { 'name': 'Tie Hua', 'title': 'senior engineer' },
            //             { 'name': 'Hei Hei', 'title': 'senior engineer',
            //               'children': [
            //                 { 'name': 'Pang Pang', 'title': 'engineer' },
            //                 { 'name': 'Xiang Xiang', 'title': 'UE engineer' }
            //               ]
            //             }
            //           ]
            //         },
            //         { 'name': 'Hong Miao', 'title': 'department manager' },
            //         { 'name': 'Chun Miao', 'title': 'department manager' }
            //       ]
            //     };

            //     var oc = $('#chart-container').orgchart({
            //       'data' : datascource,
            //       'nodeContent': 'title',
            //       'exportButton': true,
            //       'exportFilename': 'MyOrgChart',
            //       'draggable': true,
            //       'dropCriteria': function($draggedNode, $dragZone, $dropZone) {
            //         if($draggedNode.find('.content').text().indexOf('manager') > -1 && $dropZone.find('.content').text().indexOf('engineer') > -1) {
            //           return false;
            //         }
            //         return true;
            //       }
            //     });

            //     oc.$chart.on('nodedropped.orgchart', function(event) {
            //       console.log('draggedNode:' + event.draggedNode.children('.title').text()
            //         + ', dragZone:' + event.dragZone.children('.title').text()
            //         + ', dropZone:' + event.dropZone.children('.title').text()
            //       );
            //     });

            //     $('#refrescar').on('click', function(){
            //         oc.init({ 'data': datascource });
            //     });    

            /************************************* FIN ************************************************/
            // $( this ).addClass( "done" );
        });
    });

    $('body').on('click', elements.button.personalizado, function () {
        var url = Routing.generate('vista_organigrama_personalizado');
        console.log(url);
        console.log('organigrama personalizado');
    });

    // let itemStatuses = [{ id: 1, text: 'Text1' }, { id: 2, text: 'Text2' }];


    // $('#tbOrganigrama').bootstrapTable({
    //             filter: true,
    //             columns: [
    //                 [
    //                     {
    //                         title: 'ItemStatus',
    //                         field: 'ItemStatus',
    //                         sortable: true,
    //                         align: 'left',
    //                         filterType: 'select',
    //                         filterSelectData: 'var:itemStatuses'
    //                     }
    //                 ]
    //             ]
    //         });


    /*Espacio de la tabla*/
    // $('#tbOrganigrama').bootstrapTable();
    // let $itemsTable = $('#itemsTable'),
    //         itemStatuses = [{ id: 1, text: 'Text1' }, { id: 2, text: 'Text2' }];


    // $('#itemsTable').bootstrapTable();

    // $(document).ready(function () {
    //     $itemsTable.bootstrapTable({
    //         url: '../json/data.json',
    //         height: 500,
    //         filter: true,
    //         columns: [
    //             [
    //                 {
    //                     title: 'ItemID',
    //                     field: 'ItemID',
    //                     sortable: true,
    //                     align: 'center'
    //                 }, 
    //                 {
    //                     title: 'ItemName',
    //                     field: 'ItemName',
    //                     sortable: true
    //                     align: 'left',
    //                     filterType: 'input'
    //                 },
    //                 {
    //                     title: 'ItemStatus',
    //                     field: 'ItemStatus',
    //                     sortable: true
    //                     align: 'left',
    //                     filterType: 'select',
    //                     filterSelectData: 'var:itemStatuses'
    //                 }
    //             ]
    //         ]
    //     });
    // });


    // var datascource = {
    //   'name': 'Lao Lao',
    //   'title': 'general manager',
    //   'children': [
    //     { 'name': 'Bo Miao', 'title': 'department manager',
    //       'children': [{ 'name': 'Li Xin', 'title': 'senior engineer' }]
    //     },
    //     { 'name': 'Su Miao', 'title': 'department manager',
    //       'children': [
    //         { 'name': 'Tie Hua', 'title': 'senior engineer' },
    //         { 'name': 'Hei Hei', 'title': 'senior engineer',
    //           'children': [
    //             { 'name': 'Pang Pang', 'title': 'engineer' },
    //             { 'name': 'Xiang Xiang', 'title': 'UE engineer' }
    //           ]
    //         }
    //       ]
    //     },
    //     { 'name': 'Hong Miao', 'title': 'department manager' },
    //     { 'name': 'Chun Miao', 'title': 'department manager' }
    //   ]
    // };

    // var oc = $('#chart-container').orgchart({
    //   'data' : datascource,
    //   'nodeContent': 'title',
    //   'exportButton': true,
    //   'exportFilename': 'MyOrgChart',
    //   'draggable': true,
    //   'dropCriteria': function($draggedNode, $dragZone, $dropZone) {
    //     if($draggedNode.find('.content').text().indexOf('manager') > -1 && $dropZone.find('.content').text().indexOf('engineer') > -1) {
    //       return false;
    //     }
    //     return true;
    //   }
    // });

    // oc.$chart.on('nodedropped.orgchart', function(event) {
    //   console.log('draggedNode:' + event.draggedNode.children('.title').text()
    //     + ', dragZone:' + event.dragZone.children('.title').text()
    //     + ', dropZone:' + event.dropZone.children('.title').text()
    //   );
    // });

    // $('#refrescar').on('click', function(){
    //     oc.init({ 'data': datascource });
    // });    


    // $('#btn_OrgLib').on('click', function(){
    //     var rutaVistaOrgLib = Routing.generate('vista_organigrama_libre');
    //     var rutaJsonOrgLib = Routing.generate('organigrama_libre');
    //     $.ajax({
    //       url: rutaVistaOrgLib
    //     }).done(function() {
    //         console.log(rutaVistaOrgLib);
    //         window.location.href = rutaVistaOrgLib;
    //         // location.href=rutaVistaOrgLib;
    //       // $( this ).addClass( "done" );

    /************************ INICIO ***********************************************/
    // var datascource = {
    //       'name': 'Lao Lao',
    //       'title': 'general manager',
    //       'children': [
    //         { 'name': 'Bo Miao', 'title': 'department manager',
    //           'children': [{ 'name': 'Li Xin', 'title': 'senior engineer' }]
    //         },
    //         { 'name': 'Su Miao', 'title': 'department manager',
    //           'children': [
    //             { 'name': 'Tie Hua', 'title': 'senior engineer' },
    //             { 'name': 'Hei Hei', 'title': 'senior engineer',
    //               'children': [
    //                 { 'name': 'Pang Pang', 'title': 'engineer' },
    //                 { 'name': 'Xiang Xiang', 'title': 'UE engineer' }
    //               ]
    //             }
    //           ]
    //         },
    //         { 'name': 'Hong Miao', 'title': 'department manager' },
    //         { 'name': 'Chun Miao', 'title': 'department manager' }
    //       ]
    //     };

    //     var oc = $('#chart-container').orgchart({
    //       'data' : datascource,
    //       'nodeContent': 'title',
    //       'exportButton': true,
    //       'exportFilename': 'MyOrgChart',
    //       'draggable': true,
    //       'dropCriteria': function($draggedNode, $dragZone, $dropZone) {
    //         if($draggedNode.find('.content').text().indexOf('manager') > -1 && $dropZone.find('.content').text().indexOf('engineer') > -1) {
    //           return false;
    //         }
    //         return true;
    //       }
    //     });

    //     oc.$chart.on('nodedropped.orgchart', function(event) {
    //       console.log('draggedNode:' + event.draggedNode.children('.title').text()
    //         + ', dragZone:' + event.dragZone.children('.title').text()
    //         + ', dropZone:' + event.dropZone.children('.title').text()
    //       );
    //     });

    //     $('#refrescar').on('click', function(){
    //         oc.init({ 'data': datascource });
    //     });    

    /************************************* FIN ************************************************/
    // });

    //     // $.ajax({ 
    //     //     type: "GET", 
    //     //     url: rutaJsonOrgLib, 
    //     //     dataType:"json", 
    //     //     data: response, 
    //     //     success:function(response){ 
    //     //         if (response.redirect) {
    //     //             window.location.href = rutaVistaOrgLib;
    //     //         }
    //     //         else {
    //     //             // Process the expected results...
    //     //         }
    //     //     }, 
    //     //  error: function(xhr, textStatus, errorThrown) { 
    //     //         alert('Error!  Status = ' + xhr.status); 
    //     //      } 

    //     // });

    //     // var rutaJsonOrgLib = Routing.generate('organigrama_libre');
    //     // $.getJSON(rutaJsonOrgLib, function(data) {
    //     //     console.log(data);
    //     // });
    // });

});