<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class OrganigramaController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function organigramaAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('Organigrama/Organigrama.html.twig');
    }

    /**
     * @Route("/organigrama_libre",  options={"expose"=true}, name="organigrama_libre")
     */
    public function organigramaLibreAction(Request $request)
    {   
        $em                 = $this->getDoctrine()->getManager();
        $departamentoRepo   = $em->getRepository("AppBundle:Departamentos");
        $departamentos      = $departamentoRepo->obtenerDepartamento();
        // $personalDept       = $departamentoRepo->obtenerPersonalDepartamento();
        echo "<pre>";
        print_r($departamentos);
        exit;
        
            // $response = new JsonResponse();
            // $response->setData(array(
            //     'data' => $departamentos
            // ));

            // return $response;
        return $this->render('Organigrama/OrganigramaLibre.html.twig');
    }

    /**
     * @Route("/vista_organigrama_libre",  options={"expose"=true}, name="vista_organigrama_libre")
     */
    public function vistaOrganigramaLibreAction(Request $request)
    {
        // echo "Libre";
        return $this->render('Organigrama/OrganigramaLibre.html.twig');
    }

    /**
     * @Route("/vista_organigrama_personalizado",  options={"expose"=true}, name="vista_organigrama_personalizado")
     */
    public function vistaOrganigramaPersonalizadoAction(Request $request)
    {
        echo "Personalizado";
        // return $this->render('Organigrama/OrganigramaLibre.html.twig');
    }
}
