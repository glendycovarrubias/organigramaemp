<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departamentos
 *
 * @ORM\Table(name="departamentos")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DepartamentosRepository")
 */
class Departamentos
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer")
     * @ORM\ID
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $ID;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPCION", type="string", length=255)
     */
    private $DESCRIPCION;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPCION_CORTA", type="string", length=255)
     */
    private $DESCRIPCION_CORTA;

    /**
     * @var int
     *
     * @ORM\Column(name="ID_AREA", type="integer")
     */
    private $ID_AREA;

    /**
     * @var int
     *
     * @ORM\Column(name="ONE_TOOL", type="smallint")
     */
    private $ONE_TOOL;

    /**
     * @var int
     *
     * @ORM\Column(name="DEPENDENCIA_ID", type="integer")
     */
    private $DEPENDENCIA_ID;


    /**
     * Get ID
     *
     * @return int
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * Set DESCRIPCION
     *
     * @param string $DESCRIPCION
     *
     * @return Departamentos
     */
    public function setDESCRIPCION($DESCRIPCION)
    {
        $this->DESCRIPCION = $DESCRIPCION;

        return $this;
    }

    /**
     * Get DESCRIPCION
     *
     * @return string
     */
    public function getDESCRIPCION()
    {
        return $this->DESCRIPCION;
    }

    /**
     * Set DESCRIPCION_CORTA
     *
     * @param string $DESCRIPCION_CORTA
     *
     * @return Departamentos
     */
    public function setDESCRIPCION_CORTA($DESCRIPCION_CORTA)
    {
        $this->DESCRIPCION_CORTA = $DESCRIPCION_CORTA;

        return $this;
    }

    /**
     * Get DESCRIPCION_CORTA
     *
     * @return string
     */
    public function getDESCRIPCION_CORTA()
    {
        return $this->DESCRIPCION_CORTA;
    }

    /**
     * Set ID_AREA
     *
     * @param integer $ID_AREA
     *
     * @return Departamentos
     */
    public function setID_AREA($ID_AREA)
    {
        $this->ID_AREA = $ID_AREA;

        return $this;
    }

    /**
     * Get ID_AREA
     *
     * @return int
     */
    public function getID_AREA()
    {
        return $this->ID_AREA;
    }

    /**
     * Set ONE_TOO
     *
     * @param integer $ONE_TOOL
     *
     * @return Departamentos
     */
    public function setONE_TOOL($ONE_TOOL)
    {
        $this->ONE_TOOL = $ONE_TOOL;

        return $this;
    }

    /**
     * Get ONE_TOOL
     *
     * @return int
     */
    public function getONE_TOOL()
    {
        return $this->ONE_TOOL;
    }

    /**
     * Set DEPENDENCIA_ID
     *
     * @param integer $DEPENDENCIA_ID
     *
     * @return Departamentos
     */
    public function setDEPENDENCIA_ID($DEPENDENCIA_ID)
    {
        $this->DEPENDENCIA_ID = $DEPENDENCIA_ID;

        return $this;
    }

    /**
     * Get DEPENDENCIA_ID
     *
     * @return int
     */
    public function getDEPENDENCIA_ID()
    {
        return $this->DEPENDENCIA_ID;
    }
}

